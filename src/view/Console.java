package view;

import java.util.ArrayList;
import java.util.Scanner;

public class Console {
	private static Scanner scanner;

	public static String input(String msg) {
		System.out.print(msg);
		scanner = new Scanner(System.in);
		return scanner.nextLine();
	}

	public static void disp(String msg, int time) {
//		dispLine("-");
		dispMsg(msg, time);
//		dispLine("-");
//		input("↵");
	}

	public static void disp(ArrayList<String> msgList, int time) {
		dispLine("-");
		for (String msg : msgList) {
			dispMsg(msg, time);
		}
		dispLine("-");
		input("↵");
	}

	public static void dispQuestion(ArrayList<String> msgList, int time) {
		dispLine("*");
		for (String msg : msgList) {
			dispMsg(msg, time);
		}
	}

	public static void dispLine(String s) {
		int i = 1;
		while (i <= 30) {
			System.out.print(s);
			i++;
		}
		System.out.println();
	}

	private static void dispMsg(String msg, int time) {
		for (int i = 0; i < msg.length(); i++) {
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
			}
			System.out.print(msg.charAt(i));
		}
		System.out.println();
	}

}
