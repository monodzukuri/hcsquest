package rpg;

public class Game {

	public static void main(String[] args) throws Exception {

		// ここを選んだキミはキャラクター設計も担当だよ！やったね！

		Opening opening = new Opening();
		opening.start();

		Battle battle = new Battle();
		battle.start();

		Ending ending = new Ending();
		ending.start();

	}

}
