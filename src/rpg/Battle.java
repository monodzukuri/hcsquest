package rpg;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;

import view.Console;

public class Battle {

	public void start() throws MalformedURLException {

		AudioClip effect = Applet.newAudioClip(new File("bomb.wav").toURI().toURL());

		boolean flag = true;
		while (flag) {

			int no = Integer.parseInt(Console.input("コマンド？>"));
			if (no == 1) {
				effect.play(); // 1回再生
				break;
			}
		}

	}

}
