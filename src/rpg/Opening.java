package rpg;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;

public class Opening {

	public void start() throws MalformedURLException {

		AudioClip bgm = Applet.newAudioClip(new File("bgm_opening.wav").toURI().toURL());

		bgm.loop();

		View.viewOpening();

		bgm.stop();

	}

}
